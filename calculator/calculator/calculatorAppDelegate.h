//
//  calculatorAppDelegate.h
//  calculator
//
//  Created by Kai Berberich on 11/08/12.
//  Copyright (c) 2012 AXA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface calculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
