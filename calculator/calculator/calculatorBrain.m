//
//  calculatorBrain.m
//  calculator
//
//  Created by Kai Berberich on 11/08/12.
//  Copyright (c) 2012 AXA. All rights reserved.
//

#import "calculatorBrain.h"

@interface calculatorBrain()
@property (nonatomic, strong) NSMutableArray *operandStack;
@end

@implementation calculatorBrain
@synthesize operandStack = _operandStack;

-(NSMutableArray *)operandStack
{
    if ( !_operandStack) _operandStack = [ [NSMutableArray alloc] init];
    return _operandStack;
}

- (void)pushOperand:(double)operand
{
    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
}

-(double)popOperand
{
    NSNumber *operandObject = [self.operandStack lastObject];
    if (operandObject) [self.operandStack removeLastObject];
    return [operandObject doubleValue];
}

- (double)performOperation:(NSString *)operation 
{
    double result = 0;
    if ([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    }else if ([@"*" isEqualToString:operation]) {
        result = [self popOperand] * [self popOperand];
    }else if ([@"-" isEqualToString:operation]) {
        double number_1 = [self popOperand];
        double number_2 = [self popOperand];
        result = number_2 - number_1;
    }else if ([@"/" isEqualToString:operation]) {
        double number_1 = [self popOperand];
        double number_2 = [self popOperand];
        result = number_2 / number_1;
    }
        return result;
}

@end
