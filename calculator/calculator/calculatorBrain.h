//
//  calculatorBrain.h
//  calculator
//
//  Created by Kai Berberich on 11/08/12.
//  Copyright (c) 2012 AXA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface calculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)performOperation:(NSString *)operation;
@end
